(function($) {
  Drupal.behaviors.walkthru = {
    attach: function() {
      if (Drupal.settings.walkthru.continue) {
        if (Drupal.settings.walkthru.minimized) {
          Drupal.walkthru.minimize();
        }
        else {
          Drupal.walkthru.load(Drupal.settings.walkthru.name);
        }
      }
    }
  }
  
  Drupal.walkthru = Drupal.walkthru || {};
  
  Drupal.settings.walkthru = Drupal.settings.walkthru || {};
  
  Drupal.walkthru.load = function(walkthru_name) {
    var path = Drupal.settings.basePath + 'walkthru/ajax/load';
    if (typeof walkthru_name !== 'undefined') {
      path += '/' + walkthru_name;
    }
    
    $.getJSON(path, function(data) {
      if (data) {
        Drupal.settings.walkthru.data = data;
        
        var currentStep = '';
        if (typeof Drupal.settings.walkthru.step_name !== 'undefined') {
          currentStep = data[Drupal.settings.walkthru.step_name];
          Drupal.settings.walkthru.step_name = data[Drupal.settings.walkthru.step_name].next;
        }
        else {
          $.each(data, function(i, step) {
            currentStep = step;
            Drupal.settings.walkthru.step_name = step.next;
            return false;
          });
        }
        
        if (typeof Drupal.settings.walkthru.error !== 'undefined') {
          guiders.createGuider(Drupal.settings.walkthru.error);
        }
        else {
          guiders.createGuider(currentStep).show();
        }

        $.get(Drupal.settings.basePath + 'walkthru/ajax/next/' + guiders._currentGuiderID);
      }
    });
  }
  
  Drupal.walkthru.hide = function() {
    $.get(Drupal.settings.basePath + 'walkthru/ajax/close');
    delete Drupal.settings.walkthru.step_name;
    guiders.hideAll();
  }
  
  Drupal.walkthru.next = function() {
    var step = Drupal.settings.walkthru.data[Drupal.settings.walkthru.step_name];
    $.get(Drupal.settings.basePath + 'walkthru/ajax/next/' + step.id);
    
    var validated = true;
    if (typeof step.force_url !== 'undefined') {
      validated = Drupal.walkthru.validateURL(step);
    }
    
    if (validated) {
      if (typeof step.next !== 'undefined') {
        Drupal.settings.walkthru.step_name = step.next;
      }
      
      if (typeof Drupal.settings.walkthru.error !== 'undefined') {
        delete Drupal.settings.walkthru.error;
      }
      
      guiders.createGuider(step).show();
    }
  }
  
  Drupal.walkthru.minimize = function() {
    guiders.hideAll();
    $('body').prepend('<div id="walkthru-minimize">Click here to restore your walkthru.</div>');
    $.get(Drupal.settings.basePath + 'walkthru/ajax/minimize/true');
    $('#walkthru-minimize').click(function() {
      $.get(Drupal.settings.basePath + 'walkthru/ajax/minimize/false');
      $('#walkthru-minimize').remove();
      Drupal.walkthru.load();
    });
  }
  
  Drupal.walkthru.validateURL = function(step) {
    var currentURL = window.location.pathname;
    var regex = Drupal.settings.basePath + step.force_url;
    regex = regex.replace(/\*/g, "[^ ]*");
    var re = new RegExp(regex);
    var match = currentURL.match(re);

    if (match && match[0] === currentURL) {
      return true;
    }
    else {
      errorBox = new Object();
      errorBox.title = "Hmm Wrong Place";
      errorBox.description = step.force_url_error_message;
      errorBox.buttons = [{"name": "Ok I'm There", "onclick": "Drupal.walkthru.next();"},{"name": "Close", "onclick": "Drupal.walkthru.hide();"}];
      
      guiders.createGuider(errorBox).show();
      return false;
    }
  }
})(jQuery);